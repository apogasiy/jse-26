package com.tsc.apogasiy.tm.service;

import com.tsc.apogasiy.tm.api.service.IPropertyService;
import com.tsc.apogasiy.tm.exception.system.UnknownPropertyException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Optional;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull private static final String FILE_NAME = "application.properties";
    @NotNull private static final String PSWD_SECRET_KEY = "password.secret";
    @NotNull private static final String PSWD_ITER_KEY = "password.iteration";
    @NotNull private static final String APP_VER_KEY = "application.version";
    @NotNull private static final String DEV_NAME_KEY = "developer.name";
    @NotNull private static final String DEV_EMAIL_KEY = "developer.email";

    @NotNull private final Properties properties = new Properties();
    @NotNull private final HashMap<String, String> propertyDefaultMap = new HashMap<>();

    {
        propertyDefaultMap.put(PSWD_SECRET_KEY, "730333210");
        propertyDefaultMap.put(PSWD_ITER_KEY, "16553");
        propertyDefaultMap.put(APP_VER_KEY, "1.0");
        propertyDefaultMap.put(DEV_NAME_KEY, "Ivan Petrov");
        propertyDefaultMap.put(DEV_EMAIL_KEY, "ipetrov@mailbox.su");
    }

    @SneakyThrows(IOException.class)
    public PropertyService(){
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (Optional.ofNullable(inputStream).isPresent()) {
            properties.load(inputStream);
            inputStream.close();
        }
    }

    @NotNull
    private String getPropertyValue(@NotNull final String name) {
        if (!propertyDefaultMap.containsKey(name))
            throw new UnknownPropertyException();
        if (System.getProperties().containsKey(name))
            return System.getProperty(name);
        if (System.getenv().containsKey(name))
            return System.getenv(name);
        return properties.getProperty(name, propertyDefaultMap.get(name));
    }

    @Override
    @NotNull
    public String getApplicationVersion() {
        return getPropertyValue(APP_VER_KEY);
    }

    @Override
    @NotNull
    public String getDeveloperEmail() {
        return getPropertyValue(DEV_EMAIL_KEY);
    }

    @Override
    @NotNull
    public String getDeveloperName() {
        return getPropertyValue(DEV_NAME_KEY);
    }

    @Override
    @NotNull
    public String getPasswordSecret() {
        return getPropertyValue(PSWD_SECRET_KEY);
    }

    @Override
    @NotNull
    public Integer getPasswordIteration() {
        return Integer.parseInt(getPropertyValue(PSWD_ITER_KEY));
    }

}
