package com.tsc.apogasiy.tm.model;

import lombok.Getter;
import lombok.Setter;
import com.tsc.apogasiy.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class User extends AbstractEntity {

    @Getter @Setter @NotNull private String login;
    @Getter @Setter @NotNull private String hashedPassword;
    @Getter @Setter @Nullable private String email;
    @Getter @Setter @Nullable private String lastName = "";
    @Getter @Setter @Nullable private String firstName = "";
    @Getter @Setter @Nullable private String middleName = "";
    @Getter @Setter @Nullable private Role role = Role.USER;
    @Getter @Setter @NotNull private Boolean locked = false;

    public User(@NotNull final String login, @NotNull String hashedPassword) {
        this.login = login;
        this.hashedPassword = hashedPassword;
    }

}
