package com.tsc.apogasiy.tm.model;

import lombok.Getter;
import lombok.Setter;
import com.tsc.apogasiy.tm.api.entity.IWBS;
import com.tsc.apogasiy.tm.enumerated.Status;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

public class Task extends AbstractOwnerEntity implements IWBS {

    @Getter @Setter @NotNull private String name;
    @Getter @Setter @Nullable private String description;
    @Getter @Setter @Nullable private Status status = Status.NOT_STARTED;
    @Getter @Setter @Nullable private String projectId = null;
    @Getter @Setter @Nullable private Date startDate = null;
    @Getter @Setter @Nullable private Date finishDate = null;
    @Getter @Setter @Nullable private Date created = new Date();

    public Task(@NotNull final String userId, @NotNull final String name) {
        this.userId = userId;
        this.name = name;
    }

    public Task(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        setUserId(userId);
        setName(name);
        setDescription(description);
    }

    @Override
    @NotNull
    public String toString() {
        return id + ": " + name;
    }

}
