package com.tsc.apogasiy.tm.repository;

import com.tsc.apogasiy.tm.api.repository.IRepository;
import com.tsc.apogasiy.tm.model.AbstractEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull protected final List<E> list = new ArrayList<>();

    public void add(@Nullable final E entity) {
        list.add(entity);
    }

    public void remove(@Nullable final E entity) {
        list.remove(entity);
    }

    @NotNull
    public List<E> findAll() {
        return list;
    }

    @NotNull
    public List<E> findAll(@NotNull final Comparator<E> comparator) {
        list.sort(comparator);
        return list;
    }

    public void clear() {
        list.clear();
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Nullable
    public E findById(@NotNull final String id) {
        return list.stream()
                .filter(e -> e.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    @NotNull
    public E findByIndex(@NotNull final Integer index) {
        return list.get(index);
    }

    public boolean existsById(@NotNull final String id) {
        return findById(id) != null;
    }

    public boolean existsByIndex(@NotNull final Integer index) {
        return index < list.size() && index >= 0;
    }

    @Nullable
    public E removeById(@NotNull final String id) {
        @Nullable final E entity = findById(id);
        if (!Optional.ofNullable(entity).isPresent())
            return null;
        list.remove(entity);
        return entity;
    }

    @Nullable
    public E removeByIndex(@NotNull final Integer index) {
        @Nullable final E entity = findByIndex(index);
        list.remove(entity);
        return entity;
    }
}
