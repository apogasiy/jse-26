package com.tsc.apogasiy.tm.api.service;

import com.tsc.apogasiy.tm.model.Project;
import com.tsc.apogasiy.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectTaskService {

    @Nullable
    List<Task> findTaskByProjectId(@NotNull final String userId, @Nullable final String projectId);

    @Nullable
    Task bindTaskById(@NotNull final String userId, @Nullable final String projectId, @Nullable final String taskId);

    @Nullable
    Task unbindTaskById(@NotNull final String userId, @Nullable final String projectId, @Nullable final String taskId);

    @Nullable
    Project removeById(@NotNull final String userId, @NotNull final String projectId);

    @Nullable
    Project removeByIndex(@NotNull final String userId, @NotNull final Integer index);

    @Nullable
    Project removeByName(@NotNull final String userId, @NotNull final String name);

}
