package com.tsc.apogasiy.tm.api.service;

import com.tsc.apogasiy.tm.api.repository.IRepository;
import com.tsc.apogasiy.tm.model.AbstractEntity;

public interface IService <E extends AbstractEntity> extends IRepository<E> {

}
