package com.tsc.apogasiy.tm.api.repository;

import com.tsc.apogasiy.tm.command.AbstractCommand;
import com.tsc.apogasiy.tm.model.Command;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface ICommandRepository {

    @NotNull
    Collection<AbstractCommand> getCommands();

    @NotNull
    Collection<AbstractCommand> getArguments();

    @Nullable
    AbstractCommand getCommandByName(@NotNull final String name);

    @Nullable
    AbstractCommand getCommandByArg(@NotNull final String arg);

    @NotNull
    Collection<String> getCommandNames();

    @NotNull
    Collection<String> getArgNames();

    void add(@NotNull final AbstractCommand command);

}
