package com.tsc.apogasiy.tm.enumerated;

import com.tsc.apogasiy.tm.comparator.ComparatorByCreated;
import com.tsc.apogasiy.tm.comparator.ComparatorByName;
import com.tsc.apogasiy.tm.comparator.ComparatorByStartDate;
import com.tsc.apogasiy.tm.comparator.ComparatorByStatus;

import java.util.Arrays;
import java.util.Comparator;

public enum Sort {

    NAME("Sort by name", ComparatorByName.getInstance()),
    CREATED("Sort by created", ComparatorByCreated.getInstance()),
    START_DATE("Sort by start date", ComparatorByStartDate.getInstance()),
    STATUS("Sort by status", ComparatorByStatus.getInstance());

    private final String displayName;
    private final Comparator comparator;

    Sort(final String displayName, final Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

    public static boolean isValid(String sortType) {
        for (Sort sort : Sort.values()) {
            if (sortType.equals(sort.name()))
                return true;
        }
        return false;
    }

}
