package com.tsc.apogasiy.tm.command.system;

import com.jcabi.manifests.Manifests;
import com.tsc.apogasiy.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class AboutCommand extends AbstractCommand {

    @Override
    public @NotNull String getCommand() {
        return "about";
    }

    @Override
    public @Nullable String getArgument() {
        return "-a";
    }

    @Override
    public @NotNull String getDescription() {
        return "Display developer info";
    }

    @Override
    public void execute() {
        System.out.println("Developer: " + Manifests.read("developer"));
        System.out.println("e-mail: " + Manifests.read("email"));
    }

}