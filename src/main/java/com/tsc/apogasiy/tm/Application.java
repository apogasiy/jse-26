package com.tsc.apogasiy.tm;

import com.tsc.apogasiy.tm.component.Bootstrap;
import org.jetbrains.annotations.NotNull;

public class Application {

    public static void main(final String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}